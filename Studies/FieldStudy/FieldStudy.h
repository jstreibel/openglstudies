//
// Created by joao on 08/04/2021.
//

#ifndef OPENGLTUTORIAL_FIELDSTUDY_H
#define OPENGLTUTORIAL_FIELDSTUDY_H

#include <GL/glew.h>
#include "../StudiesBase.h"
#include "../../OpenGLUtils/ZoomPanRotate.h"
#include "../../util/Clock.h"
#include "../../OpenGLUtils/Program.h"
#include "../../OpenGLUtils/Drawables/VertexBuffer.h"
#include "../StudiesBase.h"
#include "../../OpenGLUtils/ZoomPanRotate.h"

#include <oglplus/program.hpp>
#include <oglplus/vertex_array.hpp>
#include <oglplus/buffer.hpp>
#include <oglplus/uniform.hpp>

#include <string>
#include <vector>
#include <chrono>


class FieldStudy : public StudiesBase {
public:
    FieldStudy(int winWidth, int winHeight);

    void doYourThing() override;

private:
    Clock clock;
    //Viewport posSpaceVP, fourierSpaceVP;

    float phase = 0;

    float xRot = 0, yRot = 0;

    //oglplus::Program lineShader;
    //oglplus::VertexArray line;
    //std::vector<GLfloat> lineData_v;
    //oglplus::Buffer lineData;

    oglplus::Uniform<GLfloat> uYPos, uXRot, uYRot, uPhase;
    oglplus::Uniform<GLboolean> uVertical;

    Program pendulumPhaseSpaceShader;
    VertexBuffer quadVertices;

    Program lineShader;
    VertexBuffer line;

    unsigned int winWidth, winHeight;
};


#endif //OPENGLTUTORIAL_FIELDSTUDY_H
