//
// Created by joao on 08/04/2021.
//

#ifndef OPENGLTUTORIAL_LEXYSTUDY_H
#define OPENGLTUTORIAL_LEXYSTUDY_H

#include <GL/glew.h>
#include "../../OpenGLUtils/Program.h"
#include "../../OpenGLUtils/Drawables/VertexBuffer.h"
#include "../StudiesBase.h"
#include "../../OpenGLUtils/ZoomPanRotate.h"
#include "../../util/Clock.h"
#include "../../util/Vector.h"
#include "../../OpenGLUtils/Drawables/PointSet.h"
#include "ChaosGameSet.h"
#include "../../OpenGLUtils/Drawables/Plane.h"

#include <nanogui/window.h>

#include <string>
#include <vector>

class LexyStudy : public StudiesBase {
    nanogui::Screen &screen;
    nanogui::Window *window = nullptr;
public:
    LexyStudy(nanogui::Screen &screen);

    void doYourThing() override;

//    void resize(GLuint newWindowWidth, GLuint newWindowHeight);

private:
    Clock clock;
    const int N;
    int nDraw = 100;

    Program pointsShader;

    float bias = .5f;
    ChaosGame chaosGame;

    int winWidth, winHeight;
};


#endif //OPENGLTUTORIAL_LEXYSTUDY_H
